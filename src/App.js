import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
    value: '',
  };

  // User Can Add a Todo:
  // When a user types into the top input and hits the Enter/return key, 
  // it should add it as a todo and empty the input. DONE

  addTodo = (event) => {
    if (event.key === 'Enter') {
      let newTodo = {
        userId: 1,
        id: Math.random() * 3698,
        title: event.target.value,
        completed: false,
      };
      let newTodosList = [...this.state.todos, newTodo];
      this.setState({ todos: newTodosList, value: '' });
    }
  };

  handleChange = (event) => {
    this.setState({ ...this.state, value: event.target.value });
  };

  // User can mark a todo as complete: DONE
  // When a user clicks on the circle at the beginning of a todo it will toggle 
  // whether that todo is completed or not. DONE

  handleToggle = (id) => {
    let newTodo = this.state.todos.map((todo) => {
      if (todo.id === id) {
        return {
          ...todo,
          completed: !todo.completed,
        }
      }
      return todo
    });

    this.setState((state) => {
      return {
        ...state,
        todos: newTodo,
      };
    })
  }
  // User Can Delete a Todo:
  // When a user clicks the "X" on the right of a Todo, 
  // it removes it from the list. DONE

  removeTodo = (event, id) => {
    const newList = this.state.todos.filter((todo) => todo.id !== id);
    this.setState({ todos: newList });
  }

  // User Can Delete all Todos Marked as Complete:
  // When a user clicks the button 'Clear Completed' it will 
  // delete all todos that are marked as complete. DONE

  clearTodoList = () => {
    const newList = this.state.todos.filter(
      (todo) => todo.completed === false
    );
    this.setState({ todos: newList });
  }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>Todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
            onKeyDown={this.addTodo}
            value={this.state.value}
            onChange={this.handleChange}
          />
        </header>
        <TodoList
          todos={this.state.todos}
          handleToggle={this.handleToggle}
          removeTodo={this.removeTodo}
          clearTodoList={this.clearTodoList}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.clearTodoList}>Clear Completed Tasks</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            onChange={this.props.handleToggle}
            checked={this.props.completed}
          />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={this.props.removeTodo} />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              key={todo.id}
              title={todo.title}
              completed={todo.completed}
              handleToggle={() => this.props.handleToggle(todo.id)}
              removeTodo={(event) => this.props.removeTodo(event, todo.id)}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
